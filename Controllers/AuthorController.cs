﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Quotes.Models;
using Quotes.ViewModels;

namespace Quotes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly IQuotesDbContext _quotesDbContext;
        private readonly IMapper _mapper;
        public AuthorController(IQuotesDbContext quotesDbContext, IMapper mapper)
        {
            _quotesDbContext = quotesDbContext;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAllAuthors()
        {
            var author = _quotesDbContext.Authors.ToList();

            var list = _mapper.Map<List<AuthorResponseViewModel>>(author);
            return Ok(list);
        }

        [HttpGet("{id}", Name = "AuthorById")]
        public IActionResult GetAuthorById(int id)
        {
            var author = _quotesDbContext.Authors.FirstOrDefault(x => x.Id == id);
            return Ok(author);
        }

        [HttpPost]
        public IActionResult Create(AuthorViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var author = _mapper.Map<Author>(model);
            author.Id= GetLastItem() + 1;
            author.CreatedAt = DateTime.Now;

            _quotesDbContext.Authors.Add(author);

            return Ok(author);
        }


        [HttpPut("{id}")]
        public IActionResult Update([FromBody]AuthorUpdateViewModel model)
        {
            var author = _quotesDbContext.Authors.FirstOrDefault(x => x.Id == model.Id);
            if (author != null)
            {
                _quotesDbContext.Authors.Remove(author);
                author.FirstName = model.FirstName;
                author.LastName = model.LastName;
                author.CreatedAt = DateTime.Now;
                _quotesDbContext.Authors.Add(author);
                return Ok(author);
            }
            return Ok("Not found by this id");
        }


        private int GetLastItem()
        {
            if (_quotesDbContext.Authors.Count > 0)
            {
                var max = _quotesDbContext.Authors.Max(x => x.Id);
                return max;
            }

            return 0;
        }


    }
}