﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Quotes.Models;
using Quotes.ViewModels;
using IMapper = AutoMapper.IMapper;

namespace Quotes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly IQuotesDbContext _quotesDbContext;
        private readonly IMapper _mapper;

        public CategoryController(IQuotesDbContext quotesDbContext, IMapper mapper)
        {
            _quotesDbContext = quotesDbContext;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAllCategories()
        {
            var category = _quotesDbContext.Categories.ToList();
            return Ok(_mapper.Map<List<CategoryResponseViewModel>>(category));
        }

        [HttpGet("{id}", Name = "CategoryById")]
        public IActionResult GetCategoryById(int id)
        {
            var category = _quotesDbContext.Categories.FirstOrDefault(x => x.Id == id);
            return Ok(category);
        }

        [HttpPost]
        public IActionResult Create([FromBody]CategoryViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var category = _mapper.Map<Category>(model);
            category.Id = GetLastItem() + 1;
            category.CreatedAt = DateTime.Now;
            _quotesDbContext.Categories.Add(category);

            return Ok(category);
        }


        [HttpPut("{id}")]
        public IActionResult Update([FromBody]CategoryUpdateViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Fields Must not be empty");
            }
            var category = _quotesDbContext.Categories.FirstOrDefault(x => x.Id == model.Id);
            if (category != null)
            {
                _quotesDbContext.Categories.Remove(category);
                category.Title = model.Title;
                category.CreatedAt = DateTime.Now;
                _quotesDbContext.Categories.Add(category);
                return Ok(category);
            }
            return Ok("Not found by this id");
        }


        private int GetLastItem()
        {
            if (_quotesDbContext.Categories.Count > 0)
            {
                var max = _quotesDbContext.Categories.Max(x => x.Id);
                return max;
            }

            return 0;

        }
    }
}