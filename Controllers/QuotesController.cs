﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Quotes.Models;
using Quotes.ViewModels;

namespace Quotes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuotesController : ControllerBase
    {
        private readonly IQuotesDbContext _quotesDbContext;
        private readonly IMapper _mapper;

        public QuotesController(IQuotesDbContext quotesDbContext, IMapper mapper)
        {
            _quotesDbContext = quotesDbContext;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAllQuotes()
        {
            var quote = _quotesDbContext.Quotes.ToList();
            return Ok(_mapper.Map<List<QuoteResponseViewModel>>(quote));
        }

        [HttpGet("{id}", Name = "QuoteById")]
        public IActionResult GetQuoteById(int id)
        {
            var quote = _quotesDbContext.Quotes.FirstOrDefault(x => x.Id == id);
            return Ok(quote);
        }

        [HttpGet("RandomQuote")]
        public IActionResult GetRandomQuote()
        {
            Random rand = new Random();
            int toSkip = rand.Next(0, _quotesDbContext.Quotes.Count);
            
            var random = _quotesDbContext.Quotes.OrderBy(r => rand.Next()).FirstOrDefault();
           // var quote = _quotesDbContext.Quotes.Skip(toSkip).Take(1).First();
            return Ok(random);
        }

        [HttpPost]
        public IActionResult Create(QuoteViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (GetAuthorById(model.AuthorId) == null || GetCategoryById(model.CategoryId) == null)
            {
                return BadRequest("Specified Category or Author doesn't exist");
            }
            var quote = _mapper.Map<Quote>(model);
            quote.Id = GetLastItem() + 1;
            quote.Category = GetCategoryById(model.CategoryId);
            quote.Author = GetAuthorById(model.AuthorId);
            quote.CreatedAt = DateTime.Now;
            _quotesDbContext.Quotes.Add(quote);
            return Ok(quote);
        }


        [HttpPut("{id}")]
        public IActionResult Update([FromBody]QuoteUpdateViewModel model)
        {

            var quote = _quotesDbContext.Quotes.FirstOrDefault(x => x.Id == model.Id);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (GetAuthorById(model.AuthorId) == null || GetCategoryById(model.CategoryId) == null)
            {
                return BadRequest("Specified Category or Author doesn't exist");
            }

            if (quote != null)
            {
                _quotesDbContext.Quotes.Remove(quote);
                var update = _mapper.Map<Quote>(model);
                update.CreatedAt = DateTime.Now;
                update.Category = GetCategoryById(model.CategoryId);
                update.Author = GetAuthorById(model.AuthorId);
                _quotesDbContext.Quotes.Add(update);
                return Ok(update);
            }
            return Ok("Not found by this id");
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var quote = _quotesDbContext.Quotes.FirstOrDefault(x => x.Id == id);
            if (quote != null)
            {
                _quotesDbContext.Quotes.Remove(quote);
                return Ok("deleted Successfully");
            }

            return Ok("Not found by this id");
        }


        private int GetLastItem()
        {

            if (_quotesDbContext.Quotes.Count > 0)
            {
                var max = _quotesDbContext.Quotes.Max(x => x.Id);
                return max;
            }

            return 0;

        }

        private Category GetCategoryById(int id)
        {
            return _quotesDbContext.Categories.FirstOrDefault(x => x.Id == id);
        }
        private Author GetAuthorById(int id)
        {
            return _quotesDbContext.Authors.FirstOrDefault(x => x.Id == id);
        }


    }
}