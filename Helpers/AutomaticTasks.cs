﻿using System.Diagnostics;
using System.Linq;
using System.Timers;
using Quotes.Models;

namespace Quotes.Helpers
{
    public class AutomaticTasks : IAutomaticTasks
    {
        private readonly IQuotesDbContext _quotesDbContext;
        private Timer timer;

        public AutomaticTasks(IQuotesDbContext quotesDbContext)
        {
            _quotesDbContext = quotesDbContext;
            timer = new Timer();
            timer.Interval =  20 * 1000;
            timer.Elapsed += Timer_Elapsed;
        }

        public void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Trace.WriteLine("Tick");
            Debug.WriteLine("Tick");
            var quotes = _quotesDbContext.Quotes.Where(x => x.CreatedAt.Hour > 1);

            foreach (var quote in quotes)
            {
                _quotesDbContext.Quotes.Remove(quote);
            }
        }
    }
}