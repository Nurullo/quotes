﻿using System.Timers;

namespace Quotes.Helpers
{
    public interface IAutomaticTasks
    {
        void Timer_Elapsed(object sender, ElapsedEventArgs e);
    }
}