﻿using System;
using System.Collections.Generic;

namespace Quotes.Models
{
    public class Author
    {
        
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CreatedAt { get; set; }
        public ICollection<Quote> Quotes  { get; set; }

    }
}