﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Quotes.Models
{
    public class Category
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime CreatedAt { get; set; }
        public ICollection<Quote> Quotes { get; set; }
    }
}