﻿using System.Collections.Generic;

namespace Quotes.Models
{
    public interface IQuotesDbContext
    {
        IList<Quote> Quotes { get;set; }
        IList<Category> Categories { get; set; }
        IList<Author> Authors { get; set; }
    }
}