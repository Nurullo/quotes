﻿using System;

namespace Quotes.Models
{
    public class Quote
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public int AuthorId { get; set; }
        public int CategoryId { get; set; }
        public DateTime CreatedAt { get; set; }
        public Category Category { get; set; }
        public Author Author { get; set; }
    }
}