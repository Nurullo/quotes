﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using Quotes.Helpers;

namespace Quotes.Models
{
    public class QuotesDbContext : IQuotesDbContext
    {
        public int test = 5;
        public QuotesDbContext()
        {
            //Authors.Add(new Author()
            //{
            //    CreatedAt = DateTime.Now,
            //    FirstName = "test",
            //    Id = 1,
            //    LastName = "TEst",
              
            //});

            Quotes = new List<Quote>();
            Categories = new List<Category>();
            Authors = new List<Author>();
        }
        public IList<Quote> Quotes { get; set; }
        public IList<Category> Categories{ get; set; }
        public IList<Author> Authors { get; set; }
    }
}