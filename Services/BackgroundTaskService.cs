﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Quotes.Models;
using System.Collections.Generic;

namespace Quotes.Services
{
    public class BackgroundTaskService : IHostedService, IDisposable
    {
        private readonly IQuotesDbContext _quotesDbContext;
        private Timer _timer;
        public BackgroundTaskService(IQuotesDbContext quotesDbContext)
        {
            _quotesDbContext = quotesDbContext;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DeleteQuotes, null, TimeSpan.Zero,
                TimeSpan.FromMinutes(1));
            return Task.CompletedTask;
        }

        public void DeleteQuotes(object state)
        {

            var quotes = _quotesDbContext.Quotes.ToList().RemoveAll(x => x.CreatedAt.Hour > 1);

        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}