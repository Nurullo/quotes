﻿using System.ComponentModel.DataAnnotations;

namespace Quotes.ViewModels
{
    public class AuthorUpdateViewModel
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
    }
}