﻿using System.ComponentModel.DataAnnotations;

namespace Quotes.ViewModels
{
    public class AuthorViewModel
    {
        
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
    }
}