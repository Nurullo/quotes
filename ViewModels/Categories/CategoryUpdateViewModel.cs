﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Quotes.Models;

namespace Quotes.ViewModels
{
    public class CategoryUpdateViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
       
    }
}