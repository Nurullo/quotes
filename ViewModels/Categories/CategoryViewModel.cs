﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Quotes.ViewModels
{
    public class CategoryViewModel
    {

        [Required]
        public string Title { get; set; }
 
    }
}