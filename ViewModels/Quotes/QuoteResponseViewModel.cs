﻿using System.ComponentModel.DataAnnotations;

namespace Quotes.ViewModels
{
    public class QuoteResponseViewModel:QuoteViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public int AuthorId { get; set; }
        public int CategoryId { get; set; }
    }
}