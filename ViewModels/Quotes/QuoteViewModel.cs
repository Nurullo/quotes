﻿using System.ComponentModel.DataAnnotations;

namespace Quotes.ViewModels
{
    public class QuoteViewModel
    {

        [Required]
        public string Title { get; set; }
        [Required]
        public string Text { get; set; }
        [Required]
        public int AuthorId { get; set; }
        [Required]
        public int CategoryId { get; set; }
    }
}